# -*- coding: utf-8 -*-
# Author : Rémi Fabre

import sys, pygame
import time
import os
import primitives
from kinematics import *
import math
from pygame.locals import *
from utils import *
import colorama
from colorama import Fore, Back, Style


class RobotUI:
    def __init__(self, robot, params):
        # Initialisation
        pygame.init()
        self.robot = robot
        self.params = params
        # Screen management
        self.width = 800
        self.height = 400
        self.black = 0, 0, 0
        self.screen = self.screen_init()
        # Mode management
        self.mode_list = [
            "dk",
            "walk",
            "ik_robot",
            "rotation",
            "ik_leg",
            "draw",
            "sinus_leg",
            "knife",
        ]
        self.is_mode_active = False
        self.mode_id = 0
        self.special_leg_id = 1  # The leg that will be selected in single leg modes
        # Walk management
        self.init_front_amplitude = 20
        self.init_lateral_amplitude = 14
        self.max_front_amplitude = 30
        self.max_lateral_amplitude = 20
        self.init_freq = 1
        self.max_freq = 2
        self.x_amplitude = self.init_front_amplitude
        self.y_amplitude = self.init_lateral_amplitude
        self.walk = primitives.WalkHolonome(self.robot, self.params)
        self.params.startFromInit = True
        self.leg_up = False
        # IK management
        self.extra_theta = 0
        # Draw management
        self.x_drawing = 180
        self.z_drawing = 30
        self.x_drawing_ground = 20
        self.z_drawing_ground = 10
        self.sinus_amplitude = 30
        # Knife management
        self.knife_speed = 1
        self.knife_max_speed = 4.0
        self.knife_init_position = [60, 0, 70]
        self.knife_speed_windup_time = 10
        self.start_chopping = False
        # Time management
        self.clock = pygame.time.Clock()
        self.max_loop_freq = 100
        self.t0 = time.time()
        self.t = self.t0
        self.keep_going = True
        # Persistent attributes
        self.x = 0
        self.y = 0
        self.z = 0
        # First print
        self.print_mode_help()

    def screen_init(self):
        screen = pygame.display.set_mode((self.width, self.height))
        screen.fill(self.black)
        chartImage = pygame.image.load("effortChart.jpg").convert()
        chartRect = chartImage.get_rect()
        chartRect.centerx = self.width / 2 + 3
        chartRect.centery = self.height / 2 + 8
        screen.blit(chartImage, chartRect)
        return screen

    def get_mode(self):
        return self.mode_list[self.mode_id]

    def next_mode(self):
        self.leg_up == False
        if self.is_mode_active:
            self.toggle_state()
        self.mode_id = (self.mode_id + 1) % len(self.mode_list)
        self.t0 = time.time()
        mode = self.get_mode()
        if mode == "draw":
            self.x = self.x_drawing_ground
            self.z = self.z_drawing_ground
        elif mode == "sinus_leg":
            None
        self.print_mode_help()

    def toggle_state(self):
        if self.is_mode_active:
            if self.get_mode() == "dk":
                self.robot.enable_torque()
            # Going back to the start position if the mode was active
            self.smooth_start_position()
            self.x = 0
            self.y = 0
            self.z = 0
            self.extra_theta = 0
            self.params.startFromInit = True
        self.is_mode_active = not (self.is_mode_active)
        if self.is_mode_active:
            # Actions that are done only once, on mode activation
            if self.get_mode() == "knife":
                self.smooth_weapon_selection()
            elif self.get_mode() == "sinus_leg":
                # Smoothly going to initial position
                angles = computeIK(
                    self.x_drawing,
                    0,
                    self.z_drawing - self.sinus_amplitude,
                )
                setAnglesToLeg(angles, self.robot.legs[self.special_leg_id])
                self.robot.smooth_tick_read_and_write(1, verbose=False)
                self.t0 = time.time()
            elif self.get_mode() == "dk":
                self.robot.disable_torque()
            print("Mode active!")
        else:
            print("Mode inactive")

    def smooth_start_position(self, delay=1):
        print("Smoothly going back to start position")
        setPositionToRobot(0, 0, 0, self.robot, self.params)
        self.robot.smooth_tick_read_and_write(delay, verbose=False)

    def smooth_weapon_selection(self, delay=2):
        setAnglesToLeg(
            computeIKNotOriented(
                self.knife_init_position[0],
                self.knife_init_position[1],
                self.knife_init_position[2],
                self.special_leg_id,
                self.params,
                verbose=False,
            ),
            self.robot.legs[self.special_leg_id],
        )
        self.robot.smooth_tick_read_and_write(delay, verbose=False)

    def tick(self):
        mode = self.get_mode()
        # The loop lasts at least (1/max_loop_freq)s
        self.clock.tick(self.max_loop_freq)
        pygame.display.update()

        # Handling keyboard, mouse and Pygame events
        for event in pygame.event.get():
            if event.type == QUIT:
                print("Closing the UI...")
                pygame.quit()
                self.keep_going = False
                break
            if event.type == KEYDOWN:
                if event.key == K_TAB:
                    self.next_mode()
                if event.key == K_RETURN:
                    self.toggle_state()
                if event.key == K_e:
                    print("Emergency stop!")
                    self.robot.disable_torque()
                    pygame.quit()
                    self.keep_going = False
                    break
            if mode == "walk":
                if event.type == KEYDOWN:
                    if event.key == K_j:
                        self.params.method = "brutal"
                        print("Walk method is now '{}'".format(self.params.method))
                    if event.key == K_k:
                        self.params.method = "constantSpeed"
                        print("Walk method is now '{}'".format(self.params.method))
                    if event.key == K_l:
                        self.params.method = "maxAccel"
                        print("Walk method is now '{}'".format(self.params.method))
                    if event.key == K_m:
                        self.params.method = "minJerk"
                        print("Walk method is now '{}'".format(self.params.method))
                    if event.key == K_s:
                        # Smooth mode
                        self.params.speed = 0.5
                        self.x_amplitude = self.max_front_amplitude
                        self.y_amplitude = self.max_lateral_amplitude
                        print("Smooth mode")
                    if event.key == K_p:
                        # Precise mode
                        self.params.speed = 3
                        self.x_amplitude = 10
                        self.y_amplitude = 10
                        print("Precise mode")
                    if event.key == K_n:
                        # Normal mode
                        self.params.speed = self.init_freq
                        self.x_amplitude = self.init_front_amplitude
                        self.y_amplitude = self.init_lateral_amplitude
                        print("Normal mode")
                    if event.key == K_LSHIFT:
                        # Turbo mode
                        self.x_amplitude = self.max_front_amplitude
                        self.y_amplitude = self.max_lateral_amplitude
                        self.params.speed = self.max_freq
                        print("Turbo mode")
                    if event.key == K_UP:
                        self.x = self.x_amplitude
                    if event.key == K_DOWN:
                        self.x = -self.x_amplitude
                    if event.key == K_RIGHT:
                        self.y = self.y_amplitude
                    if event.key == K_LEFT:
                        self.y = -self.y_amplitude
                    self.params.walkMagnitudeX = self.x
                    self.params.walkMagnitudeY = self.y

                if event.type == KEYUP:
                    if event.key == K_LSHIFT:
                        # Slow mode
                        self.x_amplitude = self.init_front_amplitude
                        self.y_amplitude = self.init_lateral_amplitude
                        self.params.speed = self.init_freq
                    if event.key == K_UP:
                        self.x = 0
                    if event.key == K_DOWN:
                        self.x = 0
                    if event.key == K_RIGHT:
                        self.y = 0
                    if event.key == K_LEFT:
                        self.y = 0
                    self.params.walkMagnitudeX = self.x
                    self.params.walkMagnitudeY = self.y

                if pygame.mouse.get_pressed()[0] == 1:
                    # [0] for left click, [2] for right click
                    if (
                        event.type == pygame.MOUSEMOTION
                        or event.type == pygame.MOUSEBUTTONDOWN
                    ):
                        xMouse, yMouse = pygame.mouse.get_pos()
                        self.x = int((xMouse - self.width / 2) * 0.2)
                        self.y = int((yMouse - self.height / 2) * 0.2)
                        self.params.walkMagnitudeX = self.x
                        self.params.walkMagnitudeY = self.y
                        self.params.activateWalk = True
                else:
                    self.params.activateWalk = False

            elif mode == "ik_robot":
                if event.type == KEYDOWN:
                    if event.key == K_UP:
                        self.z = self.z + 5
                    if event.key == K_DOWN:
                        self.z = self.z - 5
                    if event.key == K_LEFT:
                        self.extra_theta = self.extra_theta + math.pi / (2 * 10)
                    if event.key == K_RIGHT:
                        self.extra_theta = self.extra_theta - math.pi / (2 * 10)
                    if event.key == K_SPACE:
                        self.leg_up = not (self.leg_up)
                if pygame.mouse.get_pressed()[0] == 1:
                    # [0] for left click, [2] for right click
                    if (
                        event.type == pygame.MOUSEMOTION
                        or event.type == pygame.MOUSEBUTTONDOWN
                    ):
                        xMouse, yMouse = pygame.mouse.get_pos()
                        self.x = int((xMouse - self.width / 2) * 0.2)
                        self.y = int((yMouse - self.height / 2) * 0.2)
            elif mode == "draw":
                if pygame.mouse.get_pressed()[0] == 1:
                    if (
                        event.type == pygame.MOUSEMOTION
                        or event.type == pygame.MOUSEBUTTONDOWN
                    ):
                        xMouse, yMouse = pygame.mouse.get_pos()
                        self.y = -int((xMouse - self.width / 2) * 0.2)
                        self.x = self.x_drawing_ground - int(
                            (yMouse - self.height / 2) * 0.2
                        )
                if event.type == KEYDOWN:
                    if event.key == K_UP:
                        self.z = self.z + 5
                    if event.key == K_DOWN:
                        self.z = self.z - 5

            elif mode == "knife":
                if event.type == KEYDOWN:
                    if event.key == K_SPACE:
                        self.start_chopping = not (self.start_chopping)
                        print("start_chopping={}".format(self.start_chopping))
                        self.t0 = time.time()
                        self.knife_speed = 1
                    if event.key == K_s:
                        self.leg_up = not (self.leg_up)

        if self.is_mode_active:
            # Calculating positions if the current mode is active
            if mode == "dk":
                self.robot.tick_read()
                self.robot.print_dk()
            if mode == "walk":
                self.walk.run()
                self.params.startFromInit = False
            elif mode == "ik_robot":
                if self.leg_up == False:
                    setPositionToRobot(
                        self.x,
                        self.y,
                        self.z,
                        self.robot,
                        self.params,
                        extra_theta=self.extra_theta,
                    )
                else:
                    setAnglesToLeg(
                        (43, -90.76, 75.22),
                        self.robot.legs[3],
                    )
            elif mode == "rotation":
                r = 300
                angle_max = math.pi / 8
                # T = math.fmod(2 * math.pi * 0.05 * time.time(), 1)
                # if T < 0.5:
                #     angle = angle_max * T + (0.5 - T) * (-angle_max)
                # else:
                #     angle = -angle_max * T + (0.5 - T) * (angle_max)

                for leg_id in range(1, 7):
                    # if (leg_id != 3) and (leg_id != 6):
                    #     continue
                    angle = (
                        angle_max * math.sin(time.time())
                        + self.params.legAngles[leg_id - 1]
                    )
                    # print(angle)
                    x = r * math.cos(angle)
                    y = r * math.sin(angle)
                    # setAnglesToLeg(
                    #     computeIKNotOrientedRobotCentered(
                    #         60,
                    #         0.1034 * 1000 + 200 + 30 * math.cos(time.time()),
                    #         -60,
                    #         leg_id,
                    #         self.params,
                    #         verbose=False,
                    #     ),
                    #     self.robot.legs[leg_id],
                    # )
                    setAnglesToLeg(
                        computeIKNotOrientedRobotCentered(
                            x,
                            y,
                            -60,
                            leg_id,
                            self.params,
                            verbose=False,
                        ),
                        self.robot.legs[leg_id],
                    )

            elif mode == "ik_leg":
                print(
                    "Input the leg ID (1-6), and coordinates (in mm), relative to the leg's initial position. Input any letter to exit the mode."
                )
                try:
                    leg_id = int(input("leg_id: "))
                    self.x = float(input("x: "))
                    self.y = float(input("y: "))
                    self.z = float(input("z: "))
                    setAnglesToLeg(
                        computeIKNotOriented(
                            self.x,
                            self.y,
                            self.z,
                            leg_id,
                            self.params,
                            verbose=True,
                        ),
                        self.robot.legs[leg_id],
                    )
                    self.robot.smooth_tick_read_and_write(1, verbose=False)
                except:
                    print("Error parsing your input. Aborting and changing the mode")
                    self.next_mode()
            elif mode == "draw":
                setAnglesToLeg(
                    computeIKNotOriented(
                        self.x,
                        self.y,
                        self.z,
                        self.special_leg_id,
                        self.params,
                        verbose=False,
                    ),
                    self.robot.legs[self.special_leg_id],
                )
            elif mode == "sinus_leg":
                self.t = time.time() - self.t0
                self.y = 0 - self.sinus_amplitude * math.sin(
                    2 * math.pi * 0.25 * self.t
                )
                self.z = self.z_drawing - self.sinus_amplitude * math.cos(
                    2 * math.pi * 0.25 * self.t
                )
                angles = computeIK(self.x_drawing, self.y, self.z)
                setAnglesToLeg(angles, self.robot.legs[self.special_leg_id])
            elif mode == "knife" and self.start_chopping:
                self.knife_speed = min(
                    self.knife_max_speed,
                    max(
                        1,
                        self.knife_max_speed
                        * (time.time() - self.t0)
                        / self.knife_speed_windup_time,
                    ),
                )
                self.t = ((time.time() - self.t0) * 10 * self.knife_speed) // 10
                self.x = self.knife_init_position[0]
                if self.t % 3 == 1:
                    self.z = 20
                else:
                    self.z = self.knife_init_position[2]
                if self.t % 12 == 0 or self.t % 12 == 6:
                    self.y = self.knife_init_position[1]
                    self.x = 80
                elif self.t % 12 == 3:
                    self.y = -30
                    self.x = 45
                elif self.t % 12 == 9:
                    self.y = 30
                    self.x = 45
                if self.leg_up == False:
                    setAnglesToLeg(
                        computeIKNotOriented(
                            self.x,
                            self.y,
                            self.z,
                            self.special_leg_id,
                            self.params,
                            verbose=False,
                        ),
                        self.robot.legs[self.special_leg_id],
                    )
                    if time.time() - self.t0 - self.knife_speed_windup_time > 2:
                        # Auto leaving this mode if kept long enough
                        self.start_chopping = False
                        self.smooth_weapon_selection()
                else:
                    setAnglesToLeg(
                        (43, -90.76, 75.22),
                        self.robot.legs[3],
                    )
                    self.robot.tick_write()
                    self.robot.disable_torque(
                        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 16, 18]
                    )
                    input("Press enter to leave")
                    pygame.quit()
                    self.keep_going = False

        # Actually sending the write orders
        if mode != "dk":
            # Otherwise, tick_write enables the motors again.
            self.robot.tick_write()
        return self.keep_going

    def print_mode_help(self):
        prev_mode = self.mode_list[(self.mode_id - 1) % len(self.mode_list)]
        next_mode = self.mode_list[(self.mode_id + 1) % len(self.mode_list)]
        mode = self.get_mode()
        # ["walk", "ik_robot", "ik_leg", "draw", "sinus_leg", "knife"]
        print(
            "Press ENTER to activate/deactivate a mode or TAB to go to the next mode. Press E for emergency stop."
        )
        print(
            "{}Previous mode:'{}'    {}Current mode:'{}'{}    Next mode:'{}'{}".format(
                Back.CYAN + Fore.BLACK,
                prev_mode,
                Back.WHITE + Fore.BLACK,
                mode,
                Back.CYAN + Fore.BLACK,
                next_mode,
                Style.RESET_ALL,
            )
        )

        if mode == "dk":
            print(
                "Prints the current position of the motors along with the position of the tip of each leg"
            )
        elif mode == "walk":
            print(
                "Click the center of the image and drag to control the walking speed and direction of the walk. Keybindings:"
            )
            print(
                "Interpolation method press 'j' for brutal, 'k' for constant speed, 'l' for max acceleration and 'm' for the glorious min jerk "
            )
            print(
                "Walking mode presets, press 's' for smooth, 'n' for normal, 'p' for precise, 'SHIFT' for turbo"
            )
        elif mode == "ik_robot":
            print(
                "Click the center of the image and drag to control the (x, y) position of the robot. Press the UP and DOWN arrows to control (z). Press the LEFT and RIGHT arrows to control extra_theta"
            )
        elif mode == "rotation":
            print("TODO")
        elif mode == "ik_leg":
            print("TODO (enter coordinates for the leg in its own frame).")
        elif mode == "draw":
            print(
                "Put a pen on the robot's leg and click the center of the image and drag to control the (x, y) position of the robot. Press the UP and DOWN arrows to control (z)"
            )
        elif mode == "sinus_leg":
            print("Draws a circle with the tip of the leg.")
        elif mode == "knife":
            print(
                "Put a deadly weapon on the leg and press SPACE when you're ready... Press SPACE again to stop smoothly (or wait until the robot is done :D). Press S to start the surrender mode."
            )
        else:
            print("\nERROR: mode '{}' is unknown.".format(mode))
