"""
To do :
- Clean walk primitive with all the params -> Check
- Holonome walking -> Check
- Direct cinematic? -> Check
- Try the speed control loop
- write stuff with a pen? -> Check
- Use the direct kinematic to smooth down the walking gait transitions
"""

import json
import pypot.robot
import time
from kinematics import *
import primitives
import math
import sys

# import display
from timedPosition import *
from trajectoryManager import *
from pypot.dynamixel import *
from contextlib import closing
from utils import *
from signal import signal, SIGINT
from sys import exit
import logging.config
import traceback

# Uncomment this to access Pypot's loggers (https://poppy-project.github.io/pypot/logging.html)
LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "formatters": {"time": {"format": "%(asctime)s - %(levelname)s - %(message)s"}},
    "handlers": {
        "file": {
            "level": "DEBUG",
            "class": "logging.FileHandler",
            "filename": "pypot.log",
            "formatter": "time",
        }
    },
    "loggers": {
        "pypot.robot.config": {"handlers": ["file"], "level": "DEBUG"},
        "pypot.dynamixel.io": {"handlers": ["file"], "level": "DEBUG"},
    },
}

logging.config.dictConfig(LOGGING)


class RobotManager(object):
    def main(self):
        # angles = computeIK(float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]), 51, 64, 93)
        # print("Angles : " + str(angles))

        with closing(
            pypot.robot.from_json("robotConfig_bioloid.json", strict=True, sync=True)
        ) as robot:
            # Defining the shutdown function here so it has visibility over the robot variable
            def shutdown(signal_received, frame):
                # Handle any cleanup here
                print(
                    "SIGINT or CTRL-C detected. Setting motors to compliant and exiting"
                )
                allMotorsCompliant(robot)
                time.sleep(0.4)
                sys.exit()

            # Tell Python to run the handler() function when SIGINT is recieved
            signal(SIGINT, shutdown)
            try:
                print("The robot configuration is loaded")
                # do stuff without having to make sure not to forget to close robot!
                # allMotorsCompliantAndPrint(robot)
                allMotorsNotCompliant(robot)

                params = Parameters(
                    freq=50,
                    speed=1,
                    z=-60,
                    travelDistancePerStep=80,
                    lateralDistance=90,
                    frontDistance=87,
                    frontStart=32,
                    method="minJerk",
                )

                print("Setting initial position")
                # initPositionForWalk(robot, params)
                setPositionToRobot(0, 0, 0, robot, params)
                time.sleep(0.5)
                print("Moving a bit")
                t0 = time.time()
                while True:
                    setPositionToRobot(
                        20 * math.cos(2 * math.pi * 0.5 * (time.time() - t0)),
                        20 * math.sin(2 * math.pi * 0.5 * (time.time() - t0)),
                        0,
                        robot,
                        params,
                    )
                    time.sleep(0.005)

                return
                # debug(robot, params)
                display.createDisplay(robot, params)
                # testPrimitive(robot, params)
                # self.walk(robot, 1)
                # walk = primitives.WalkPrimitive(robot, params)
                # while True :
                #     try:
                #         method = input('Method (minJerk, constantSpeed, brutal) ? : ')
                #         if (len(method) < 2) :
                #             method = "constantSpeed"

                #         endAsInit = input('End as init (y, *) ? : ')
                #         if (endAsInit == "y") :
                #             endToInit = True
                #         else :
                #             endToInit = False
                #     except ValueError:
                #         print("default values used")
                #         method = "constantSpeed"
                #         endToInit = False

                #     params.method = method
                #     params.endToInit = endToInit
                #     walk.start()
                #     walk.wait_to_stop()
                #     if (endToInit) :
                #         params.startFromInit = True
                #     else :
                #         params.startFromInit = False

                time.sleep(2)
                initPositionForWalk(robot, params)
                return

                # self.walk(robot, 1)
                # self.walkDebugTripod(robot)

                return
                walk = primitives.WalkPrimitive(robot, 3)
                walk.start()
                walk.wait_to_stop()
                # self.duckScorpionPosition(robot)
                # self.walkingMovement(robot)
            except Exception as e:
                track = traceback.format_exc()
                print(track)
            finally:
                shutdown(None, None)

    def walk(
        self,
        robot,
        speed=1,
        z=-60,
        travelDistancePerStep=80,
        lateralDistance=90,
        frontDistance=90,
        frontStart=32,
    ):
        # Putting 3 legs in the air
        robot.motor_12.goal_position = robot.motor_12.present_position - 20
        robot.motor_32.goal_position = robot.motor_32.present_position - 20
        robot.motor_52.goal_position = robot.motor_52.present_position - 20
        time.sleep(0.1 * speed)

        # Set1 of legs go forward in the air
        anglesLeg1 = computeIK(lateralDistance, -travelDistancePerStep / 2, z + 30)
        anglesLeg3 = computeIK(frontStart, frontDistance, z + 30)
        anglesLeg5 = computeIK(
            frontStart + travelDistancePerStep, -frontDistance, z + 30
        )
        setAnglesToLeg(anglesLeg1, robot.leg1)
        setAnglesToLeg(anglesLeg3, robot.leg3)
        setAnglesToLeg(anglesLeg5, robot.leg5)
        time.sleep(0.15 * speed)

        while True:
            # Set1 of legs touch the ground
            anglesLeg1 = computeIK(lateralDistance, -travelDistancePerStep / 2, z)
            anglesLeg3 = computeIK(frontStart, frontDistance, z)
            anglesLeg5 = computeIK(
                frontStart + travelDistancePerStep, -frontDistance, z
            )
            setAnglesToLeg(anglesLeg1, robot.leg1)
            setAnglesToLeg(anglesLeg3, robot.leg3)
            setAnglesToLeg(anglesLeg5, robot.leg5)
            time.sleep(0.1 * speed)

            # Set2 of legs go in the air
            anglesLeg4 = computeIK(lateralDistance, -travelDistancePerStep / 2, z + 30)
            anglesLeg2 = computeIK(
                frontStart + travelDistancePerStep, -frontDistance, z + 30
            )
            anglesLeg6 = computeIK(frontStart, frontDistance, z + 30)
            setAnglesToLeg(anglesLeg4, robot.leg4)
            setAnglesToLeg(anglesLeg6, robot.leg6)
            setAnglesToLeg(anglesLeg2, robot.leg2)
            time.sleep(0.1 * speed)

            # Set2 of legs go forward in the air
            anglesLeg4 = computeIK(lateralDistance, travelDistancePerStep / 2, z + 30)
            anglesLeg2 = computeIK(frontStart, -frontDistance, z + 30)
            anglesLeg6 = computeIK(
                frontStart + travelDistancePerStep, frontDistance, z + 30
            )
            setAnglesToLeg(anglesLeg4, robot.leg4)
            setAnglesToLeg(anglesLeg6, robot.leg6)
            setAnglesToLeg(anglesLeg2, robot.leg2)
            # Set1 of legs go backwards
            anglesLeg1 = computeIK(lateralDistance, travelDistancePerStep / 2, z)
            anglesLeg3 = computeIK(frontStart + travelDistancePerStep, frontDistance, z)
            anglesLeg5 = computeIK(frontStart, -frontDistance, z)
            setAnglesToLeg(anglesLeg1, robot.leg1)
            setAnglesToLeg(anglesLeg3, robot.leg3)
            setAnglesToLeg(anglesLeg5, robot.leg5)
            time.sleep(0.15 * speed)

            # Set2 of legs touch the ground
            anglesLeg4 = computeIK(lateralDistance, travelDistancePerStep / 2, z)
            anglesLeg2 = computeIK(frontStart, -frontDistance, z)
            anglesLeg6 = computeIK(frontStart + travelDistancePerStep, frontDistance, z)
            setAnglesToLeg(anglesLeg4, robot.leg4)
            setAnglesToLeg(anglesLeg6, robot.leg6)
            setAnglesToLeg(anglesLeg2, robot.leg2)
            time.sleep(0.1 * speed)

            # Set1 of legs go in the air
            anglesLeg1 = computeIK(lateralDistance, travelDistancePerStep / 2, z + 30)
            anglesLeg3 = computeIK(
                frontStart + travelDistancePerStep, frontDistance, z + 30
            )
            anglesLeg5 = computeIK(frontStart, -frontDistance, z + 30)
            setAnglesToLeg(anglesLeg1, robot.leg1)
            setAnglesToLeg(anglesLeg3, robot.leg3)
            setAnglesToLeg(anglesLeg5, robot.leg5)
            time.sleep(0.1 * speed)

            # Set1 of legs go forward in the air
            anglesLeg1 = computeIK(lateralDistance, -travelDistancePerStep / 2, z + 30)
            anglesLeg3 = computeIK(frontStart, frontDistance, z + 30)
            anglesLeg5 = computeIK(
                frontStart + travelDistancePerStep, -frontDistance, z + 30
            )
            setAnglesToLeg(anglesLeg1, robot.leg1)
            setAnglesToLeg(anglesLeg3, robot.leg3)
            setAnglesToLeg(anglesLeg5, robot.leg5)
            # Set2 of legs go backwards
            anglesLeg4 = computeIK(lateralDistance, -travelDistancePerStep / 2, z)
            anglesLeg2 = computeIK(
                frontStart + travelDistancePerStep, -frontDistance, z
            )
            anglesLeg6 = computeIK(frontStart, frontDistance, z)
            setAnglesToLeg(anglesLeg4, robot.leg4)
            setAnglesToLeg(anglesLeg6, robot.leg6)
            setAnglesToLeg(anglesLeg2, robot.leg2)
            time.sleep(0.15 * speed)

    def initPositionBackwards(self, robot):

        for m in robot.shoulders:
            m.compliant = False
            m.goal_position = 0

        for m in robot.elbows:
            m.compliant = False
            m.goal_position = 65

        for m in robot.wrists:
            m.compliant = False
            m.goal_position = 65

        # Left arms
        robot.motor_21.goal_position = -30
        robot.motor_31.goal_position = 30
        # Right arms
        robot.motor_51.goal_position = -30
        robot.motor_61.goal_position = 30

    def duckScorpionPosition(self, robot):
        for m in robot.motors:
            m.compliant = False
            m.goal_position = 20
        # Left arms
        robot.motor_21.goal_position = -20
        robot.motor_31.goal_position = 20
        # Right arms
        robot.motor_51.goal_position = -20
        robot.motor_61.goal_position = 20
        # Head
        robot.motor_11.goal_position = 0
        robot.motor_12.compliant = True
        robot.motor_13.compliant = True
        # Tail
        robot.motor_41.goal_position = 0
        robot.motor_42.compliant = True
        robot.motor_43.compliant = True

    def autoDetectRobot(self):
        print("Robot detection ...")
        robot = autodetect_robot()
        print("Robot detected. Writing to conf file ...")
        config = robot.to_config()
        print(config)
        with open("robotConfigAuto.json", "wb") as f:
            json.dump(config, f)
        print("Robot conf file written...")

    def loadRobotConf(self):
        return from_json("robotConfig.json")


print("A new day dawns")
robMan = RobotManager()
# robMan.autoDetectRobot()
print("Starting main...")
robMan.main()
# robMan.loadRobotConf()
# robMan.createRobotConf()
# robot = robMan.readRobotConf()
# robMan.close()
print("Done !")

