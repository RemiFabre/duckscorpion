import numpy
import math
import scipy.optimize as opt


globFinalPos = 0
globMaxAcceleration = 0
globInitSpeed = 0
globFinalSpeed = 0


def minJerk(
    initial, final, duration, init_vel=0.0, init_acc=0.0, final_vel=0.0, final_acc=0.0
):
    """ Simple function computing the minimum jerk coefs and returning a lambda function.
    The general equation is: x(t) = a0 + a1 * t + a2 * t**2 + a3 * t**3 + a4 * t**4 + a5 * t**5
    Considering xi, xf, dotxi, dotxf, dotdotxi and dotdotxf being respectively the initial position, final position
    initial velocity, final velocity, initial acceleration and final acceleration.
    We have to solve the system:

    a3*d**3+a4*d**4+a5*d**5=xf-a0-a1*d-a2*d**2
    3*a3*d**2+4*a4*d**3+5*a5*d**4=dotxf-a1-2*a2*d
    6*a3*d+12*a4*d**2+20*a5*d**3=dotdotxf-2*a2

    """

    a0 = initial
    a1 = init_vel
    a2 = init_acc / 2.0

    d5 = duration ** 5
    d4 = duration ** 4
    d3 = duration ** 3
    d2 = duration ** 2
    A = numpy.array(
        [[d3, d4, d5], [3 * d2, 4 * d3, 5 * d4], [6 * duration, 12 * d2, 20 * d3]]
    )
    B = numpy.array(
        [
            final - a0 - (a1 * duration) - (a2 * d2),
            final_vel - a1 - (2 * a2 * duration),
            final_acc - (2 * a2),
        ]
    )

    # Solves AX + B = 0
    X = numpy.linalg.solve(A, B)

    # print((a0, " ", a1, " ", a2, " ", X[0], " ", X[1], " ", X[2]))
    return (
        lambda x: a0
        + a1 * x
        + a2 * x ** 2
        + X[0] * x ** 3
        + X[1] * x ** 4
        + X[2] * x ** 5
    )


# Simple primitive with a constant speed (infinite acceleration at the begin and end though)
def constantSpeed(distance, totalTime):
    return lambda x: (distance / totalTime) * x


# Brutally gives the destination point
def brutal(distance):
    return lambda x: distance


def sign(x):
    return math.copysign(1, x)


# finalPos is suposed to be positive
def maxAccelMano(
    finalPos, maxAcceleration, maxSpeed, initSpeed=0, finalSpeed=None, printMode=False
):
    a2 = maxAcceleration / 2
    a1 = initSpeed
    a0 = 0
    # tms = Time to get to max speed with a positive bang. ts = Time to start the negative bang. T = total trajectory time
    tms = (maxSpeed - initSpeed) / (2 * a2)

    function1 = lambda t: a0 + a1 * t + a2 * t * t
    functionMaxSpeed = lambda t: (t - tms) * maxSpeed + function1(tms)

    if finalSpeed == None or finalSpeed >= maxSpeed:
        T = (-a1 + math.sqrt(a1 * 2 + 4 * finalPos * a2)) / (2 * a2)
        ts = T
        # A single second degree polynome + a constant function will do the trick
        b0 = a0
        b1 = a1
        b2 = a2
    else:
        # We probably need two second degree polynomes to solve this
        b2 = -a2
        # Solved with sympy (works)
        Xf = finalPos
        vf = finalSpeed
        [b1, b0, ts, T] = [
            (
                -a1 * a2
                + math.sqrt(2) * math.sqrt(a2 ** 2 * (4 * Xf * a2 + a1 ** 2 + vf ** 2))
            )
            / a2,
            (
                -4 * Xf * a2 ** 2
                - 3 * a1 ** 2 * a2
                + 2
                * math.sqrt(2)
                * a1
                * math.sqrt(a2 ** 2 * (4 * Xf * a2 + a1 ** 2 + vf ** 2))
                - a2 * vf ** 2
            )
            / (4 * a2 ** 2),
            (
                -2 * a1 * a2
                + math.sqrt(2) * math.sqrt(a2 ** 2 * (4 * Xf * a2 + a1 ** 2 + vf ** 2))
            )
            / (4 * a2 ** 2),
            (
                -a2 * (a1 + vf)
                + math.sqrt(2) * math.sqrt(a2 ** 2 * (4 * Xf * a2 + a1 ** 2 + vf ** 2))
            )
            / (2 * a2 ** 2),
        ]

        if T < 0 or ts < 0:
            [b1, b0, ts, T] = [
                -(
                    a1 * a2
                    + math.sqrt(2)
                    * math.sqrt(a2 ** 2 * (4 * Xf * a2 + a1 ** 2 + vf ** 2))
                )
                / a2,
                -(
                    4 * Xf * a2 ** 2
                    + 3 * a1 ** 2 * a2
                    + 2
                    * math.sqrt(2)
                    * a1
                    * math.sqrt(a2 ** 2 * (4 * Xf * a2 + a1 ** 2 + vf ** 2))
                    + a2 * vf ** 2
                )
                / (4 * a2 ** 2),
                -(
                    2 * a1 * a2
                    + math.sqrt(2)
                    * math.sqrt(a2 ** 2 * (4 * Xf * a2 + a1 ** 2 + vf ** 2))
                )
                / (4 * a2 ** 2),
                -(
                    a2 * (a1 + vf)
                    + math.sqrt(2)
                    * math.sqrt(a2 ** 2 * (4 * Xf * a2 + a1 ** 2 + vf ** 2))
                )
                / (2 * a2 ** 2),
            ]

        if ts > tms:
            # The solution implies a bigger speed than tolerated, hence we need to limit it
            delta_T_ts = abs(
                (maxSpeed - finalSpeed) / (2 * b2)
            )  # Time to get from max speed to final speed
            ts = (
                tms
                + (finalPos - delta_T_ts * (maxSpeed + finalSpeed) / 2 - function1(tms))
                / maxSpeed
            )
            b1 = maxSpeed  # Speed continuity
            b0 = function1(tms) + (ts - tms) * maxSpeed  # Position continuity
            T = delta_T_ts + ts
        else:
            tms = ts

    function2 = lambda t: b0 + b1 * (t - ts) + b2 * (t - ts) ** 2

    pwFunction = lambda t: numpy.piecewise(
        numpy.array([t]),
        [
            numpy.array([t]) < tms,
            numpy.array([t]) >= tms and numpy.array([t]) <= ts,
            numpy.array([t]) > ts,
        ],
        [function1, functionMaxSpeed, function2],
    )[0]
    if printMode == False:
        return [pwFunction, T]
    else:
        # Useful if you want to compare position vs speed vs accel
        speed1 = lambda t: a1 + 2 * a2 * t
        speedMaxSpeed = lambda t: maxSpeed
        speed2 = lambda t: b1 + 2 * b2 * (t - ts)
        pwSpeed = lambda t: numpy.piecewise(
            numpy.array([t]),
            [
                numpy.array([t]) < tms,
                numpy.array([t]) >= tms and numpy.array([t]) <= ts,
                numpy.array([t]) > ts,
            ],
            [speed1, speedMaxSpeed, speed2],
        )[0]
        accel1 = lambda t: 2 * a2
        accelMaxSpeed = lambda t: 0
        accel2 = lambda t: 2 * b2
        pwAccel = lambda t: numpy.piecewise(
            numpy.array([t]),
            [
                numpy.array([t]) < tms,
                numpy.array([t]) >= tms and numpy.array([t]) <= ts,
                numpy.array([t]) > ts,
            ],
            [accel1, accelMaxSpeed, accel2],
        )[0]
        return [pwFunction, pwSpeed, pwAccel, tms, ts, T]


# Used for the numeric solving of the limited accel problem
def fAccel(variables):
    a2 = globMaxAcceleration * sign(globFinalPos) / 2
    a1 = globInitSpeed
    a0 = 0
    b2 = -a2

    (b0, b1, ts, T) = variables

    eq1 = b1 + 2 * b2 * T - globFinalSpeed  # Final speed
    eq2 = b0 + b1 * T + b2 * (T ** 2) - globFinalPos  # Final position
    eq3 = (
        a0 + a1 * ts + a2 * (ts ** 2) - b0 - b1 * ts - b2 * (ts ** 2)
    )  # poly1(ts) == poly2(ts) -> position is continuous
    eq4 = (
        a1 + 2 * a2 * ts - b1 - 2 * b2 * ts
    )  # poly1'(ts) == poly2'(ts) -> speed is continuous

    # print "b0 = " + str(b0) + ", b1 = " + str(b1)
    # print abs(eq1) + abs(eq2) + abs(eq3) + abs(eq4)
    return [eq1, eq2, eq3, eq4]


def iAmAnArrayISwear(x):
    return [x]


if __name__ == "__main__":
    # We just call opt passing the function and an initial value
    # solution = opt.fsolve(f,(0.1,0))
    # print "The solution is",solution

    jerk = minJerk(0, 2048, 1)
    vector = numpy.linspace(0, 1, 100)
    for x in vector:
        print((x, " ", jerk(x)))

    # solution = maxAccelMano(100, 4, 10, 0, 0, printMode=True)

    # tms = solution[3]
    # ts = solution[4]
    # T = solution[5]

    # # print "tms = ", tms
    # # print "ts = ", ts
    # # print "T = ", T

    # vector = numpy.linspace(0, T, 100)
    # for x in vector :
    #     print x, " ", solution[0](x), " ", solution[1](x), " ", solution[2](x)

    # T = solution[3]
    # ts = solution[2]
    # print "T = ", T
    # print "ts = ", ts

    # vector = numpy.linspace(0, T, 100)
    # for x in vector :
    #     if (x <= ts) :
    #         print x, solution[0](x), solution[4](x), solution[6](x)
    #     else :
    #         print x, solution[1](x), solution[5](x), solution[7](x)

# def test(x) :
#     x = numpy.linspace(x, x, 1)
#

# class PiecewiseFunction():
#     def __init__(self, separationValues, functions) :
#         self.conditions = conditions
# 	self.separationValues = separationValues

#     def eval(self, x) :
#         x = numpy.linspace(x, x, 1)

#         conditions = []
#         for v in separationValues :
#             conditions.append(x < v)
#         lastSeparation = separationValues[len(separationValues) - 1]
#         conditions.append(x >= lastSeparation)

#         return numpy.piecewise(x, conditions, functions)[0]
