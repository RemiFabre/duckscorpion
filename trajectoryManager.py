from timedPosition import *
from timedAngles import *
from formalCalc import *


class TrajectoryManager:
    def createTrajectory(self, currentPos, destPos, params, duration=None):
        detailedAngles = []
        method = params.method
        distance = destPos.distanceToPosition(currentPos)
        if duration == None:
            deltaT = destPos.timeToPosition(currentPos)
        else:
            # Use this to re-use positions without needing to update their "t" component
            deltaT = duration
        if distance < 0.00001:
            return [
                lambda X: currentPos.x,
                lambda Y: currentPos.y,
                lambda Z: currentPos.z,
                deltaT,
            ]

        if method == "minJerk":
            # Calculating the min jerk between the 2 points in 1 dimension
            function = minJerk(0, distance, deltaT)
        elif method == "constantSpeed":
            function = constantSpeed(distance, deltaT)
        elif method == "brutal":
            function = brutal(distance)
        elif method == "maxAccel":
            maxAccel = maxAccelMano(
                distance, params.maxAccel, params.maxSpeed, 0, 0, False
            )
            function = maxAccel[0]
            deltaT = maxAccel[1]

        deltaX = destPos.x - currentPos.x
        deltaY = destPos.y - currentPos.y
        deltaZ = destPos.z - currentPos.z

        return [
            lambda X: currentPos.x + (function(X) / distance) * deltaX,
            lambda Y: currentPos.y + (function(Y) / distance) * deltaY,
            lambda Z: currentPos.z + (function(Z) / distance) * deltaZ,
            deltaT,
        ]
