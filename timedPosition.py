import math


class TimedPosition:
    def __init__(self, x, y, z, time):
        self.x = x
        self.y = y
        self.z = z
        self.time = time

    def distanceToPosition(self, pos):
        deltaX = self.x - pos.x
        deltaY = self.y - pos.y
        deltaZ = self.z - pos.z
        return math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)

    def timeToPosition(self, pos):
        return abs(self.time - pos.time)
