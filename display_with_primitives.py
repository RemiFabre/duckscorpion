# -*- coding: utf-8 -*-
# Author : Rémi Fabre

import sys, pygame
import time
import os
import primitives
from kinematics import *
import math
from pygame.locals import *
from utils import *


def createDisplay(robot, params):
    # Initialisations
    pygame.init()

    WIDTH = 800
    HEIGHT = 400
    black = 0, 0, 0
    white = 255, 255, 255

    initFrontAmplitude = 20
    initLateralAmplitude = 14
    maxFrontAmplitude = 30
    maxLateralAmplitude = 20

    initFreq = 1
    maxFreq = 2

    xAmplitude = initFrontAmplitude
    yAmplitude = initLateralAmplitude

    # Screen creation
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    black = 0, 0, 0
    screen.fill(black)

    chartImage = pygame.image.load("effortChart.jpg").convert()
    chartRect = chartImage.get_rect()

    chartRect.centerx = WIDTH / 2 + 3
    chartRect.centery = HEIGHT / 2 + 8
    screen.blit(chartImage, chartRect)

    clock = pygame.time.Clock()

    x = 0
    y = 0
    z = 0

    xDrawing = 130
    yDrawing = 0
    zDrawing = 80

    xDrawingGround = 100
    yDrawingGround = 0
    zDrawingGround = -30

    currentTime = 0
    oldTime = 0
    t0 = 0
    sign = 1

    mode = "knife"
    knifeMode = "OFF"
    walk = primitives.WalkHolonomePrimitive(robot, params)
    params.startFromInit = True
    walk.start()

    while 1:
        clock.tick(50)  # => the loop lasts at least 1/50s

        pygame.display.update()

        if mode == "walking":
            # walk.wait_to_stop()
            if walk.running == False:
                walk.start()
        elif mode == "drawing" and drawingMode == "SINUS":
            currentTime = time.time() - t0
            zSinus = zDrawing - 10 * math.sin(currentTime / 10)
            ySinus = yDrawing - 10 * math.cos(currentTime / 10)
            angles = computeIK(xDrawing + 15, ySinus, zSinus)
            setAnglesToLeg(angles, robot.leg1)
        elif mode == "knife" and knifeMode == "ON":
            currentTime = ((time.time() - t0) * 10 * 8) // 10
            x = 210
            if currentTime % 3 == 1:
                z = 25
            else:
                z = -30
            if currentTime % 12 == 0:
                y = 0
            elif currentTime % 12 == 3:
                y = -45
            elif currentTime % 12 == 6:
                y = 0
            elif currentTime % 12 == 9:
                y = 45

            angles = computeIK(x, y, z)
            setAnglesToLeg(angles, robot.leg1)

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
            if mode == "centerOfMass":
                if event.type == KEYDOWN:
                    if event.key == K_UP:
                        x = x + 5
                    if event.key == K_DOWN:
                        x = x - 5
                    if event.key == K_RIGHT:
                        y = y + 5
                    if event.key == K_LEFT:
                        y = y - 5
                    if event.key == K_TAB:
                        mode = "drawing"
                        drawingMode = "OFF"
                        print("Mode : " + str(mode))
                        print(" Drawing mode : " + str(drawingMode))
                        print("Press enter to start drawing")
                    setPositionToRobot(x, y, z, robot, params)
                if pygame.mouse.get_pressed()[0] == 1:
                    # [0] for left click, [2] for right click
                    if (
                        event.type == pygame.MOUSEMOTION
                        or event.type == pygame.MOUSEBUTTONDOWN
                    ):
                        xMouse, yMouse = pygame.mouse.get_pos()
                        x = int((xMouse - WIDTH / 2) * 0.2)
                        y = int((yMouse - HEIGHT / 2) * 0.2)
                        setPositionToRobot(x, y, z, robot, params)
            elif mode == "drawing":
                if event.type == KEYDOWN:
                    if event.key == K_TAB:
                        mode = "walking"
                        x = 0
                        y = 0
                        z = 0
                        print("Mode : " + str(mode))
                    if event.key == K_RETURN:
                        if drawingMode == "OFF":
                            drawingMode = "ON"
                            x = xDrawingGround
                            y = yDrawingGround
                            z = zDrawingGround
                        elif drawingMode == "ON":
                            drawingMode = "SINUS"
                            t0 = time.time()
                        elif drawingMode == "SINUS":
                            drawingMode = "OFF"
                            x = 0
                            y = 0
                            z = 0
                            setPositionToRobot(x, y, z, robot, params)
                        print("drawingMode : " + str(drawingMode))
                if drawingMode == "ON":
                    if (
                        event.type == pygame.MOUSEMOTION
                        or event.type == pygame.MOUSEBUTTONDOWN
                    ):
                        # print pygame.mouse.get_pressed()
                        if pygame.mouse.get_pressed()[0] == 1:
                            # x = xDrawing + 20
                            z = zDrawingGround - 15
                        else:
                            z = zDrawingGround
                            # x = xDrawing
                        xMouse, yMouse = pygame.mouse.get_pos()
                        # y = yDrawing - int((xMouse - WIDTH/2)*0.1)
                        # z = zDrawing - int((yMouse - HEIGHT/2)*0.1)
                        x = xDrawingGround - int((xMouse - WIDTH / 2) * 0.4)
                        y = yDrawingGround - int((yMouse - HEIGHT / 2) * 0.4)

                    angles = computeIK(x, y, z)
                    position = computeDK(angles[0], angles[1], angles[2])
                    # print ("Asked position : X = " + str(x) + ", Y = " + str(y) + ", Z = " + str(z))
                    print(
                        (
                            "DK : X = "
                            + str(position[0])
                            + ", Y = "
                            + str(position[1])
                            + ", Z = "
                            + str(position[2])
                        )
                    )
                    print(("Angles : ", angles))

                    setAnglesToLeg(angles, robot.leg1)

            elif mode == "walking":
                if event.type == KEYDOWN:
                    if event.key == K_TAB:
                        mode = "knife"
                        print("Mode : " + str(mode))
                        t0 = time.time()
                    if event.key == K_RETURN:
                        try:
                            method = input(
                                "Method (minJerk, constantSpeed, brutal, maxAccel) ? : "
                            )
                            if len(method) < 2:
                                method = "constantSpeed"
                            speed = float(input("Speed (default is 1.0) ? : "))
                        except ValueError:
                            print("default values used")
                            speed = 1.0

                        params.method = method
                        params.speed = speed
                    if event.key == K_s:
                        # Smooth mode
                        params.speed = 0.5
                        xAmplitude = maxFrontAmplitude
                        yAmplitude = maxLateralAmplitude
                    if event.key == K_p:
                        # Precise mode
                        params.speed = 3
                        xAmplitude = 10
                        yAmplitude = 10
                    if event.key == K_n:
                        # Normal mode
                        params.speed = initFreq
                        xAmplitude = initFrontAmplitude
                        yAmplitude = initLateralAmplitude
                    if event.key == K_LSHIFT:
                        # Turbo mode
                        xAmplitude = maxFrontAmplitude
                        yAmplitude = maxLateralAmplitude
                        params.speed = maxFreq
                    if event.key == K_UP:
                        x = xAmplitude
                    if event.key == K_DOWN:
                        x = -xAmplitude
                    if event.key == K_RIGHT:
                        y = yAmplitude
                    if event.key == K_LEFT:
                        y = -yAmplitude
                    params.walkMagnitudeX = x
                    params.walkMagnitudeY = y

                if event.type == KEYUP:
                    if event.key == K_LSHIFT:
                        # Slow mode
                        xAmplitude = initFrontAmplitude
                        yAmplitude = initLateralAmplitude
                        params.speed = initFreq
                    if event.key == K_UP:
                        x = 0
                    if event.key == K_DOWN:
                        x = 0
                    if event.key == K_RIGHT:
                        y = 0
                    if event.key == K_LEFT:
                        y = 0
                    params.walkMagnitudeX = x
                    params.walkMagnitudeY = y

                if pygame.mouse.get_pressed()[0] == 1:
                    # [0] for left click, [2] for right click
                    if (
                        event.type == pygame.MOUSEMOTION
                        or event.type == pygame.MOUSEBUTTONDOWN
                    ):
                        xMouse, yMouse = pygame.mouse.get_pos()
                        x = int((xMouse - WIDTH / 2) * 0.2)
                        y = int((yMouse - HEIGHT / 2) * 0.2)
                        print(str(x) + ", " + str(y))
                        params.walkMagnitudeX = x
                        params.walkMagnitudeY = y

                params.startFromInit = False
                if abs(x) + abs(y) >= 2:
                    params.activateWalk = True
                else:
                    params.activateWalk = False

            elif mode == "knife":
                if event.type == KEYDOWN:
                    if event.key == K_TAB:
                        mode = "centerOfMass"
                        print("Mode : " + str(mode))
                        t0 = time.time()
                        x = 0
                        y = 0
                        z = 0
                    if event.key == K_RETURN:
                        if knifeMode == "OFF":
                            knifeMode = "ON"
                            print("knifeMode : " + str(knifeMode))
                            x = xDrawingGround
                            y = yDrawingGround
                            z = zDrawingGround
                        elif knifeMode == "ON":
                            knifeMode = "OFF"
                            print("knifeMode : " + str(knifeMode))
                            # Disabled in 2019
                            # x = 0
                            # y = 0
                            # z = 0
                            # setPositionToRobot(x, y, z, robot, params)
