import pypot.dynamixel
import time
import math

ports = pypot.dynamixel.get_available_ports()
dxl_io = pypot.dynamixel.DxlIO(ports[0], baudrate=1000000)


dt = 0
t0 = time.time()
while dt < 10:
    dt = time.time() - t0
    extra = 15 * math.sin(2 * 3.1415 * 0.5 * dt)
    dxl_io.set_goal_position(
        {8: extra, 7: extra, 1: extra, 2: extra, 13: extra, 14: extra}
    )
    time.sleep(0.005)
