import time
import kinematics
import pypot.primitive
import formalCalc
from timedPosition import *
from trajectoryManager import *
from utils import *
from constants import *

# Follow a trajectory on tripod1 and an other trajectory on tripod2 at the same time (duration must match though
def followTrajTripods(robot, params, traj1, traj2):
    t0 = time.time()
    t = 0
    while t < traj1[3]:  # [3] contains the duration of the trajectory
        t = (time.time() - t0) * params.speed
        setPositionToTripod1(traj1[0](t), traj1[1](t), traj1[2](t), robot, params)
        setPositionToTripod2(traj2[0](t), traj2[1](t), traj2[2](t), robot, params)
        if USING_SIMPLE_ROBOT:
            # This is a hack to use the SimpleRobot instead of Pypot's solution
            robot.tick_write()
            if robot.__class__.__name__ == "SimpleRobotSimulation":
                robot.tickSim()


def followTrajTripod1(robot, params, traj1, auto_send=True):
    t0 = time.time()
    t = 0
    while t < traj1[3]:  # [3] contains the duration of the trajectory
        t = (time.time() - t0) * params.speed
        setPositionToTripod1(traj1[0](t), traj1[1](t), traj1[2](t), robot, params)
        if USING_SIMPLE_ROBOT and auto_send:
            # This is a hack to use the SimpleRobot instead of Pypot's solution
            robot.tick_write()
            if robot.__class__.__name__ == "SimpleRobotSimulation":
                robot.tickSim()
        else:
            time.sleep(1.0 / params.freq)


def followTrajTripod2(robot, params, traj2, auto_send=True):
    t0 = time.time()
    t = 0
    while t < traj2[3]:  # [3] contains the duration of the trajectory
        t = (time.time() - t0) * params.speed
        setPositionToTripod2(traj2[0](t), traj2[1](t), traj2[2](t), robot, params)
        if USING_SIMPLE_ROBOT and auto_send:
            # This is a hack to use the SimpleRobot instead of Pypot's solution
            robot.tick_write()
            if robot.__class__.__name__ == "SimpleRobotSimulation":
                robot.tickSim()
        else:
            time.sleep(1.0 / params.freq)


class WalkHolonome:
    def __init__(self, robot, params):
        self.robot = robot
        self.params = params

    def run(self):
        if self.params.activateWalk == False:
            time.sleep(0.02)
            return
        freq = self.params.freq
        speed = self.params.speed
        z = self.params.z
        travelDistancePerStep = self.params.travelDistancePerStep
        lateralDistance = self.params.lateralDistance
        frontDistance = self.params.frontDistance
        frontStart = self.params.frontStart

        initialPos = TimedPosition(0, 0, 0, 0)
        backwardGroundPos = TimedPosition(
            -self.params.walkMagnitudeX, -self.params.walkMagnitudeY, 0, 0
        )  # BGP
        backwardAirPos = TimedPosition(
            -self.params.walkMagnitudeX, -self.params.walkMagnitudeY, 10, 0
        )  # BAP
        forwardGroundPos = TimedPosition(
            self.params.walkMagnitudeX, self.params.walkMagnitudeY, 0, 0
        )  # FGP
        forwardAirPos = TimedPosition(
            self.params.walkMagnitudeX, self.params.walkMagnitudeY, 10, 0
        )  # FAP

        # Creating all the needed trajectories
        trajManager = TrajectoryManager()
        # TODO how can these delays be hard coded when there is a gazilion speed/freq variables already?
        # TODO this should not be a blocking function. It should return a state for a given t.
        trajInitToBGP = trajManager.createTrajectory(
            initialPos, backwardGroundPos, self.params, 0.2
        )
        trajInitToFAP = trajManager.createTrajectory(
            initialPos, forwardAirPos, self.params, 0.2
        )
        trajBAPToInit = trajManager.createTrajectory(
            backwardAirPos, initialPos, self.params, 0.2
        )
        trajFGPToInit = trajManager.createTrajectory(
            forwardGroundPos, initialPos, self.params, 0.2
        )

        trajFAPToFGP = trajManager.createTrajectory(
            forwardAirPos, forwardGroundPos, self.params, 0.1
        )
        trajFGPToBGP = trajManager.createTrajectory(
            forwardGroundPos, backwardGroundPos, self.params, 0.3
        )
        trajBGPToBAP = trajManager.createTrajectory(
            backwardGroundPos, backwardAirPos, self.params, 0.1
        )
        trajBAPToFAP = trajManager.createTrajectory(
            backwardAirPos, forwardAirPos, self.params, 0.3
        )

        if self.params.startFromInit:
            # Tripod1 init->FAP while tripod2 init->BGP
            followTrajTripods(self.robot, self.params, trajInitToFAP, trajInitToBGP)

        # Invert the 2 following lines and ...
        # Tripod2 BGP->BAP
        followTrajTripod2(self.robot, self.params, trajBGPToBAP)
        # Tripod1 FAP->FGP
        followTrajTripod1(self.robot, self.params, trajFAPToFGP)

        # Tripod1 FGP->BGP while tripod2 BAP->FAP
        followTrajTripods(self.robot, self.params, trajFGPToBGP, trajBAPToFAP)

        # ... and invert these 2 lines to get some badass walking spider
        # Tripod1 BGP->BAP
        followTrajTripod1(self.robot, self.params, trajBGPToBAP)
        # Tripod2 FAP->FGP
        followTrajTripod2(self.robot, self.params, trajFAPToFGP)

        if self.params.endToInit:
            # Tripod1 BAP->init while tripod2 FGP->init
            followTrajTripods(self.robot, self.params, trajBAPToInit, trajFGPToInit)
        else:
            # Tripod1 BAP->FAP while tripod2 FGP->BGP
            followTrajTripods(self.robot, self.params, trajBAPToFAP, trajFGPToBGP)


# Classes that use the pypot's primitives
class WalkHolonomePrimitive(pypot.primitive.Primitive):
    def __init__(self, robot, params):
        # pypot.primitive.Primitive.__init__(self, robot)
        super(WalkHolonomePrimitive, self).__init__(robot)
        self.params = params

    def run(self):
        if self.params.activateWalk == False:
            time.sleep(0.02)
            return
        freq = self.params.freq
        speed = self.params.speed
        z = self.params.z
        travelDistancePerStep = self.params.travelDistancePerStep
        lateralDistance = self.params.lateralDistance
        frontDistance = self.params.frontDistance
        frontStart = self.params.frontStart

        initialPos = TimedPosition(0, 0, 0, 0)
        backwardGroundPos = TimedPosition(
            -self.params.walkMagnitudeX, -self.params.walkMagnitudeY, 0, 0
        )  # BGP
        backwardAirPos = TimedPosition(
            -self.params.walkMagnitudeX, -self.params.walkMagnitudeY, 10, 0
        )  # BAP
        forwardGroundPos = TimedPosition(
            self.params.walkMagnitudeX, self.params.walkMagnitudeY, 0, 0
        )  # FGP
        forwardAirPos = TimedPosition(
            self.params.walkMagnitudeX, self.params.walkMagnitudeY, 10, 0
        )  # FAP

        # Creating all the needed trajectories
        trajManager = TrajectoryManager()
        trajInitToBGP = trajManager.createTrajectory(
            initialPos, backwardGroundPos, self.params, 0.2
        )
        trajInitToFAP = trajManager.createTrajectory(
            initialPos, forwardAirPos, self.params, 0.2
        )
        trajBAPToInit = trajManager.createTrajectory(
            backwardAirPos, initialPos, self.params, 0.2
        )
        trajFGPToInit = trajManager.createTrajectory(
            forwardGroundPos, initialPos, self.params, 0.2
        )

        trajFAPToFGP = trajManager.createTrajectory(
            forwardAirPos, forwardGroundPos, self.params, 0.1
        )
        trajFGPToBGP = trajManager.createTrajectory(
            forwardGroundPos, backwardGroundPos, self.params, 0.3
        )
        trajBGPToBAP = trajManager.createTrajectory(
            backwardGroundPos, backwardAirPos, self.params, 0.1
        )
        trajBAPToFAP = trajManager.createTrajectory(
            backwardAirPos, forwardAirPos, self.params, 0.3
        )

        if self.params.startFromInit:
            # Tripod1 init->FAP while tripod2 init->BGP
            followTrajTripods(self.robot, self.params, trajInitToFAP, trajInitToBGP)

        # Invert the 2 following lines and ...
        # Tripod2 BGP->BAP
        followTrajTripod2(self.robot, self.params, trajBGPToBAP)
        # Tripod1 FAP->FGP
        followTrajTripod1(self.robot, self.params, trajFAPToFGP)

        # Tripod1 FGP->BGP while tripod2 BAP->FAP
        followTrajTripods(self.robot, self.params, trajFGPToBGP, trajBAPToFAP)

        # ... and invert these 2 lines to get some badass walking spider
        # Tripod1 BGP->BAP
        followTrajTripod1(self.robot, self.params, trajBGPToBAP)
        # Tripod2 FAP->FGP
        followTrajTripod2(self.robot, self.params, trajFAPToFGP)

        if self.params.endToInit:
            # Tripod1 BAP->init while tripod2 FGP->init
            followTrajTripods(self.robot, self.params, trajBAPToInit, trajFGPToInit)
        else:
            # Tripod1 BAP->FAP while tripod2 FGP->BGP
            followTrajTripods(self.robot, self.params, trajBAPToFAP, trajFGPToBGP)


class TestPrimitive(pypot.primitive.Primitive):
    def __init__(self, robot, params):
        # pypot.primitive.Primitive.__init__(self, robot)
        super(TestPrimitive, self).__init__(robot)
        self.params = params

    def run(self):
        freq = self.params.freq
        speed = self.params.speed
        z = self.params.z
        travelDistancePerStep = self.params.travelDistancePerStep
        lateralDistance = self.params.lateralDistance
        frontDistance = self.params.frontDistance
        frontStart = self.params.frontStart
        method = self.params.method

        t0 = time.time()
        t = 0
        prevPos = TimedPosition(90, 0, -30, 0)
        destPos = TimedPosition(90, 80, -50, 2)
        trajManager = TrajectoryManager()
        traj = trajManager.createTrajectory(prevPos, destPos, self.params)
        i = 0
        while t < prevPos.timeToPosition(destPos):
            i = i + 1
            t = time.time() - t0
            # print ("Time = " + str(t))
            angles = computeIK(traj[0](t), traj[1](t), traj[2](t))
            self.robot.motor_41.goal_position = angles[0]
            self.robot.motor_42.goal_position = angles[1]
            self.robot.motor_43.goal_position = angles[2]
            time.sleep(1.0 / freq)

        traj = trajManager.createTrajectory(destPos, prevPos, self.params)
        t0 = time.time()
        t = 0
        while t < prevPos.timeToPosition(destPos):
            t = time.time() - t0
            # print ("Time = " + str(t))
            angles = computeIK(traj[0](t), traj[1](t), traj[2](t))
            self.robot.motor_41.goal_position = angles[0]
            self.robot.motor_42.goal_position = angles[1]
            self.robot.motor_43.goal_position = angles[2]
            time.sleep(1.0 / freq)


class WalkPrimitive(pypot.primitive.Primitive):
    def __init__(self, robot, params):
        # pypot.primitive.Primitive.__init__(self, robot)
        super(WalkPrimitive, self).__init__(robot)
        self.params = params

    def run(self):
        freq = self.params.freq
        speed = self.params.speed
        z = self.params.z
        travelDistancePerStep = self.params.travelDistancePerStep
        lateralDistance = self.params.lateralDistance
        frontDistance = self.params.frontDistance
        frontStart = self.params.frontStart
        method = self.params.method

        initialPos = TimedPosition(0, 0, 0, 0)
        backwardGroundPos = TimedPosition(-travelDistancePerStep / 2, 0, 0, 0)  # BGP
        backwardAirPos = TimedPosition(-travelDistancePerStep / 2, 0, 20, 0)  # BAP
        forwardGroundPos = TimedPosition(travelDistancePerStep / 2, 0, 0, 0)  # FGP
        forwardAirPos = TimedPosition(travelDistancePerStep / 2, 0, 20, 0)  # FAP

        # Creating all the needed trajectories
        trajManager = TrajectoryManager()
        trajInitToBGP = trajManager.createTrajectory(
            initialPos, backwardGroundPos, self.params, 0.2
        )
        trajInitToFAP = trajManager.createTrajectory(
            initialPos, forwardAirPos, self.params, 0.2
        )
        trajBAPToInit = trajManager.createTrajectory(
            backwardAirPos, initialPos, self.params, 0.2
        )
        trajFGPToInit = trajManager.createTrajectory(
            forwardGroundPos, initialPos, self.params, 0.2
        )

        trajFAPToFGP = trajManager.createTrajectory(
            forwardAirPos, forwardGroundPos, self.params, 0.2
        )
        trajFGPToBGP = trajManager.createTrajectory(
            forwardGroundPos, backwardGroundPos, self.params, 0.4
        )
        trajBGPToBAP = trajManager.createTrajectory(
            backwardGroundPos, backwardAirPos, self.params, 0.2
        )
        trajBAPToFAP = trajManager.createTrajectory(
            backwardAirPos, forwardAirPos, self.params, 0.4
        )

        if self.params.startFromInit:
            # Tripod1 init->FAP while tripod2 init->BGP
            followTrajTripods(self.robot, self.params, trajInitToFAP, trajInitToBGP)

        # Invert the 2 following lines and ...
        # Tripod2 BGP->BAP
        followTrajTripod2(self.robot, self.params, trajBGPToBAP)
        # Tripod1 FAP->FGP
        followTrajTripod1(self.robot, self.params, trajFAPToFGP)

        # Tripod1 FGP->BGP while tripod2 BAP->FAP
        followTrajTripods(self.robot, self.params, trajFGPToBGP, trajBAPToFAP)

        # ... and invert these 2 lines to get some badass walking spider
        # Tripod1 BGP->BAP
        followTrajTripod1(self.robot, self.params, trajBGPToBAP)
        # Tripod2 FAP->FGP
        followTrajTripod2(self.robot, self.params, trajFAPToFGP)

        if self.params.endToInit:
            # Tripod1 BAP->init while tripod2 FGP->init
            followTrajTripods(self.robot, self.params, trajBAPToInit, trajFGPToInit)
        else:
            # Tripod1 BAP->FAP while tripod2 FGP->BGP
            followTrajTripods(self.robot, self.params, trajBAPToFAP, trajFGPToBGP)
