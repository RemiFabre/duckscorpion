import pypot.dynamixel
import time
import math

ports = pypot.dynamixel.get_available_ports()
dxl_io = pypot.dynamixel.DxlIO(ports[0], baudrate=1000000)

try:
    while True:
        # Splitting the orders to avoid a voltage drop...
        dxl_io.set_goal_position(
            {
                11: 0.0,
                12: 0.0,
                13: 0.0,
                21: 0.0,
                22: 0.0,
                23: 0.0,
            }
        )
        time.sleep(0.1)
        dxl_io.set_goal_position(
            {
                31: 0.0,
                32: 0.0,
                33: 0.0,
                41: 0.0,
                42: 0.0,
                43: 0.0,
            }
        )
        time.sleep(0.1)
        dxl_io.set_goal_position(
            {
                51: 0.0,
                52: 0.0,
                53: 0.0,
                61: 0.0,
                62: 0.0,
                63: 0.0,
            }
        )
        time.sleep(0.1)
finally:
    dxl_io.disable_torque([254])
