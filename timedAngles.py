class TimedAngles:
    def __init__(self, angles, time):
        self.angles = angles
        self.time = time

    def __repr__(self):
        return "Angles : " + str(self.angles) + " Time : " + str(self.time) + "\n"
