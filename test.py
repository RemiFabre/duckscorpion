import json
import pypot.robot
import time
from kinematics import *
import primitives
import math
import sys
import display
from timedPosition import *
from trajectoryManager import *
from pypot.dynamixel import *
from contextlib import closing
from utils import *
from signal import signal, SIGINT
from sys import exit
import logging.config


robot = pypot.robot.from_json("robotConfig_bioloid.json", strict=True, sync=True)
params = Parameters(
    freq=50,
    speed=1,
    z=-60,
    travelDistancePerStep=80,
    lateralDistance=90,
    frontDistance=87,
    frontStart=32,
    method="minJerk",
)

print("allMotorsCompliantAndPrint")
allMotorsCompliantAndPrint(robot)
time.sleep(2)
print("initPositionForWalk")
initPositionForWalk(robot, params, delay=0.0)
time.sleep(2)
print("allMotorsNotCompliant")
allMotorsNotCompliant(robot)
