import kinematics
import time
import pypot.dynamixel
import math


def test_dk():
    ports = pypot.dynamixel.get_available_ports()
    dxl_io = pypot.dynamixel.DxlIO(ports[0], baudrate=1000000)
    dxl_io.disable_torque([11, 12, 13])

    while True:
        angles = dxl_io.get_present_position([11, 12, 13])
        print("raw angles: {}".format(angles))
        pos = kinematics.computeDK(angles[0], angles[1], angles[2])
        print("DK: {}".format(pos))
        print("********************\n")
        time.sleep(0.1)


def test_ik():
    ports = pypot.dynamixel.get_available_ports()
    dxl_io = pypot.dynamixel.DxlIO(ports[0], baudrate=1000000)

    angles = kinematics.computeIK(180, 0.0, 90)
    print("IK: {}".format(angles))
    dxl_io.set_goal_position({11: angles[0], 12: angles[1], 13: angles[2]})


def test_all():
    ports = pypot.dynamixel.get_available_ports()
    dxl_io = pypot.dynamixel.DxlIO(ports[0], baudrate=1000000)
    dxl_io.disable_torque([11, 12, 13])

    while True:
        angles = dxl_io.get_present_position([11, 12, 13])
        print("raw angles: {}".format(angles))
        pos = kinematics.computeDK(angles[0], angles[1], angles[2])
        print("DK: {}".format(pos))
        angles = kinematics.computeIK(pos[0], pos[1], pos[2])
        print("IK: {}".format(angles))
        print("*************************************************\n")
        time.sleep(0.1)


def circle(r, theta):
    return r * math.cos(theta), r * math.sin(theta)


def circle_demo():
    ports = pypot.dynamixel.get_available_ports()
    dxl_io = pypot.dynamixel.DxlIO(ports[0], baudrate=1000000)

    while True:
        t = time.time()
        x, y = circle(50, 2 * math.pi * 0.5 * t)
        # angles = kinematics.computeIK(200 + x, y, 0)
        angles = kinematics.computeIK(230, x, y)
        # print("IK: {}".format(angles))
        dxl_io.set_goal_position({11: angles[0], 12: angles[1], 13: angles[2]})
        time.sleep(0.01)


if __name__ == "__main__":
    test_dk()
    #     test_ik()
    # test_all()
#     circle_demo()
